/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.vitaaps.assetmaintenance;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

class DatabaseCreation {

    String user = "grain", // SQL login details;
            pass = "123",
            url = "jdbc:mysql://localhost:3306/?allowMultiQueries=true";

    private final String firstTables = "CREATE DATABASE IF NOT EXISTS Assets;" // Initial Database.
            + "USE Assets;" // ALTER TABLE Siloses CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci; - To accept cyrillics.
            + "CREATE TABLE IF NOT EXISTS Siloses " // Parent table.
            + "(Id INT PRIMARY KEY AUTO_INCREMENT, "
            + "Name VARCHAR (20));"
            + "CREATE TABLE IF NOT EXISTS Info " // Child table.
            + "(Id INT PRIMARY KEY AUTO_INCREMENT, "
            + "SiloId INT,"
            + "Cleaned CHAR(10), "
            + "Probes CHAR(10), "
            + "InternalScrew CHAR(10),"
            + "ExternalScrew CHAR(10), "
            + "Engine CHAR(10),"
            + "FOREIGN KEY(SiloId) REFERENCES Siloses(Id) ON DELETE CASCADE)";
    
    void newDatabase() {                                                        // New database if nonexistant.

        try (Connection con = DriverManager.getConnection(url, user, pass);
                PreparedStatement pst = con.prepareStatement(firstTables)) {

            boolean isResult = pst.execute();
            System.out.println("New database: " + isResult);
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(AssetMaintenance.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

class SiloUI extends DatabaseCreation {

    JPanel infoControls = new JPanel(),
            siloControlsPnl = new JPanel(),
            allControlsPnl = new JPanel(new BorderLayout()),
            titleControlsPnl = new JPanel(),
            cleaningPanel = new JPanel(new BorderLayout()), // Cleanup control.
            probesPnl = new JPanel(new BorderLayout()),
            internScrewPnl = new JPanel(new BorderLayout()),
            externScrewPnl = new JPanel(new BorderLayout()),
            checkEnginePnl = new JPanel(new BorderLayout()),
            footer = new JPanel();                                              // NOT DECIDED.

    JMenuBar menubar = new JMenuBar();
    JMenu file = new JMenu("Меню");
    JMenuItem exit = new JMenuItem("Изход");

    JButton addBtn = new JButton("Добави силоз"), // Silo Info controls.
            removeBtn = new JButton("Премахни силоз"),
            // Silo Parameters;
            cleanBtn = new JButton("Изчистване"),
            probesBtn = new JButton("Термосонди - подвързване"),
            internScrBtn = new JButton("Вътрешен шнек инспекция"),
            externScrBtn = new JButton("Външен шнек инспекция"),
            checkEngineBtn = new JButton("Двигател/ремъци инспекция");

    JComboBox siloList = new JComboBox();

    JLabel externScrewLbl = new JLabel(), // Silo Info Labels.
            internScrewLbl = new JLabel(),
            probesLbl = new JLabel(),
            checkEngineLbl = new JLabel(),
            cleanupLbl = new JLabel(),
            titleControlsLbl = new JLabel(),
            cleaningLbl = new JLabel(),
            dateTimeFooter = new JLabel();
    Object src;                                                                 // Controls Params identificator.
    String date;

    SiloUI() {

        // Menubar;
        file.add(exit);
        menubar.add(file);
        // Adding controls for silo parameters.

        cleaningPanel.setBorder(new LineBorder(Color.white, 10));               // Cleaning button&label.
        cleaningPanel.add(cleanBtn, BorderLayout.SOUTH);

        cleanupLbl.setOpaque(true);
        cleanupLbl.setBackground(Color.white);
        cleanupLbl.setForeground(Color.black);
        cleanupLbl.setFont(new Font("Verdana", Font.PLAIN, 12));
        cleanupLbl.setBounds(20, 270, 100, 25);

        probesPnl.setBorder(new LineBorder(Color.green, 10));                   // Probes button&label.
        probesPnl.add(probesBtn, BorderLayout.SOUTH);

        probesLbl.setOpaque(true);
        probesLbl.setBackground(Color.green);
        probesLbl.setForeground(Color.white);
        probesLbl.setFont(new Font("Verdana", Font.PLAIN, 12));
        probesLbl.setBounds(100, 120, 100, 25);

        internScrewPnl.setBorder(new LineBorder(Color.orange, 10));             // Internal Screw button&label.
        internScrewPnl.add(internScrBtn, BorderLayout.SOUTH);

        internScrewLbl.setOpaque(true);
        internScrewLbl.setBackground(Color.orange);
        internScrewLbl.setForeground(Color.white);
        internScrewLbl.setFont(new Font("Verdana", Font.PLAIN, 12));
        internScrewLbl.setBounds(100, 200, 100, 25);

        externScrewPnl.setBorder(new LineBorder(Color.blue, 10));               // External Screw button&label.
        externScrewPnl.add(externScrBtn, BorderLayout.SOUTH);

        externScrewLbl.setOpaque(true);
        externScrewLbl.setBackground(Color.blue);
        externScrewLbl.setForeground(Color.white);
        externScrewLbl.setFont(new Font("Verdana", Font.PLAIN, 12));
        externScrewLbl.setBounds(200, 240, 100, 25);

        checkEnginePnl.setBorder(new LineBorder(Color.red, 10));                // Check Engine button&label. 
        checkEnginePnl.add(checkEngineBtn, BorderLayout.SOUTH);

        checkEngineLbl.setOpaque(true);
        checkEngineLbl.setBackground(Color.red);
        checkEngineLbl.setForeground(Color.white);
        checkEngineLbl.setFont(new Font("Verdana", Font.PLAIN, 12));
        checkEngineLbl.setBounds(200, 270, 100, 25);

        titleControlsPnl.setBorder(new EtchedBorder(EtchedBorder.RAISED));
        titleControlsPnl.setLayout(new BorderLayout());                         // Controls title.
        titleControlsLbl.setFont(new Font("Courier", Font.PLAIN, 18));
        titleControlsLbl.setText("ПОДДРЪЖКА");
        titleControlsPnl.add(titleControlsLbl, BorderLayout.CENTER);
        titleControlsLbl.setHorizontalAlignment(JLabel.CENTER);

        infoControls.setLayout(new GridLayout(6, 1));                           // Adding components to main panel.
        infoControls.add(titleControlsPnl);
        infoControls.add(cleaningPanel);
        infoControls.add(probesPnl);
        infoControls.add(internScrewPnl);
        infoControls.add(externScrewPnl);
        infoControls.add(checkEnginePnl);

        siloControlsPnl.setLayout(new BorderLayout());// DONE
        siloControlsPnl.add(siloList, BorderLayout.NORTH);
        siloControlsPnl.add(addBtn, BorderLayout.CENTER);
        siloControlsPnl.add(removeBtn, BorderLayout.EAST);
        siloControlsPnl.setBorder(new TitledBorder("Силози"));

        allControlsPnl.add(siloControlsPnl, BorderLayout.NORTH);
        allControlsPnl.add(infoControls, BorderLayout.CENTER);
        allControlsPnl.setBorder(new BevelBorder(BevelBorder.RAISED));

        addBtn.addActionListener(new AddSilo());
        removeBtn.addActionListener(new RemoveSilo());
        siloList.addMouseListener(new UpdateSiloList());                        // Update on new silo.
        siloList.addActionListener(new SQLRecordsToLabels());
        cleanBtn.addActionListener(new SiloParams());                           // Changes silo properties.
        probesBtn.addActionListener(new SiloParams());
        internScrBtn.addActionListener(new SiloParams());
        externScrBtn.addActionListener(new SiloParams());
        checkEngineBtn.addActionListener(new SiloParams());

    }

    class AddSilo implements ActionListener {                                   // Adding siloses to table and combobox.

        @Override
        public void actionPerformed(ActionEvent e) {

            String url = "jdbc:mysql://localhost:3306/Assets?allowMultiQueries=true",
                    siloName = JOptionPane.showInputDialog(null, "Въведете силоз:", "Силоз - /До 20 символа/", JOptionPane.QUESTION_MESSAGE),
                    sql = "INSERT INTO Siloses (Name) VALUES (?)";              // The prepared statement.
                    long countInput = siloName.chars().count();                 // Checks user input length.
            if (siloName != null) {
                if (siloName.equals("") | countInput > 20) {                    // Input can't be empty or over 20 chars.

                    JOptionPane.showMessageDialog(null, "Полето е празно или сте въвели над 20 символа!", "Грешка!", JOptionPane.ERROR_MESSAGE);

                } else {

                    try (Connection con = DriverManager.getConnection(url, user, pass);
                            PreparedStatement pst = con.prepareStatement(sql)) {

                        pst.setString(1, siloName);
                        int affectedRows = pst.executeUpdate();

                        if (affectedRows == 0) {                                // Rows affected.
                            JOptionPane.showMessageDialog(null, "Няма промяна в данните!", "Грешка!", JOptionPane.ERROR_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(null, "Силозът е добавен!", null, JOptionPane.INFORMATION_MESSAGE);
                        }

                    } catch (SQLException ex) {
                        Logger.getLogger(SiloUI.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            } else {
                JOptionPane.showMessageDialog(null, "Добавянето на силоз отменено", "Добавяне", JOptionPane.INFORMATION_MESSAGE);
            }

        }

    }

    class RemoveSilo implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            String url = "jdbc:mysql://localhost:3306/Assets?",
                    siloListSel = siloList.getSelectedItem().toString(),        // Delete silo by picking a name from the list.
                    sql = "DELETE FROM Siloses WHERE (Name) = ?";               // The prepared statement.

            if (siloListSel != null) {
                if (siloListSel.equals("")) {

                    JOptionPane.showMessageDialog(null, "Не сте въвели име на силоз", "Грешка!", JOptionPane.ERROR_MESSAGE);

                } else {

                    try (Connection con = DriverManager.getConnection(url, user, pass);
                            PreparedStatement pst = con.prepareStatement(sql)) {

                        pst.setString(1, siloListSel);
                        int deleted = pst.executeUpdate();                      // Number of rows affected;

                        if (deleted == 0) {
                            JOptionPane.showMessageDialog(null, "Няма такъв силоз!", "Грешка!", JOptionPane.ERROR_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(null, "Силозът е премахнат", "Премахване силоз", JOptionPane.INFORMATION_MESSAGE);
                        }

                    } catch (SQLException ex) {
                        Logger.getLogger(DatabaseCreation.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Премахването на силоз отменено", "Отмяна", JOptionPane.INFORMATION_MESSAGE);
            }
        }

    }

    class UpdateSiloList extends MouseAdapter {

        List<String> lista = new ArrayList();

        @Override
        public void mouseEntered(MouseEvent e) {

            lista.clear();
            siloList.removeAllItems();
            String urlCombo = "jdbc:mysql://localhost:3306/Assets";
            String query = "SELECT * FROM Siloses";
            lista.add("Изберете силоз");

            try (Connection con = DriverManager.getConnection(urlCombo, user, pass);
                    PreparedStatement fill = con.prepareStatement(query);
                    ResultSet rs = fill.executeQuery()) {

                while (rs.next()) {
                    String name = rs.getString("Name");
                    lista.add(name);
                }
                for (String str : lista) {
                    siloList.addItem(str);
                }

            } catch (SQLException ex) {
                Logger.getLogger(SiloUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    class SiloParams implements ActionListener {

        JFrame change;                                                          // Frame for changing parameters.
        JPanel changePnl;
        JComboBox pickDate;
        DateTimeFormatter formatter;
        List<String> dates;                                                     // Dates of the previous 3 days.
        JButton updateBtn = new JButton("<html><b>Обнови</b></html>"),
                close = new JButton("Затвори");

        SiloParams() {
            change = new JFrame("Поддръжка");
            changePnl = new JPanel(new BorderLayout());
            pickDate = new JComboBox();
            dates = new ArrayList<>();
            formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            dates.add(LocalDate.now().format(formatter));
            dates.add(LocalDate.now().minusDays(1).format(formatter));
            dates.add(LocalDate.now().minusDays(2).format(formatter));

            pickDate.setModel(new DefaultComboBoxModel(dates.toArray()));
            date = LocalDate.now().format(formatter);
            pickDate.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    date = (String) pickDate.getSelectedItem();
                }

            });
            close.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    change.dispose();
                }
            });

        }

        @Override
        public void actionPerformed(ActionEvent ef) {
            src = ef.getSource();                                               // Param buttons identificator.

            changePnl.setBorder(new TitledBorder("Дата на промяна:"));
            changePnl.add(pickDate, BorderLayout.WEST);
            changePnl.add(updateBtn, BorderLayout.CENTER);
            changePnl.add(close, BorderLayout.EAST);
            change.add(changePnl, BorderLayout.CENTER);

            updateBtn.addActionListener(new UpdateButton());
            change.setLocationRelativeTo(null);
            change.pack();
            change.setVisible(true);

        }

    }

    class UpdateButton extends SiloParams implements ActionListener {           // Update siloList on pressing button Update.

        String urlChange = "jdbc:mysql://localhost:3306/Assets";

        @Override
        public void actionPerformed(ActionEvent e) {
                                                                                // Update or Insert Statements;
            try (Connection con = DriverManager.getConnection(urlChange, user, pass)) {

                if (src == cleanBtn) {                                          // One listener for all button.
                    String cleanInsert = "INSERT INTO Info (Id,SiloId,Cleaned) VALUES (?,?,?)"
                            + " ON DUPLICATE KEY UPDATE Cleaned = VALUES (Cleaned)";
                    PreparedStatement pstIns = con.prepareStatement(cleanInsert);
                    pstIns.setInt(1, siloList.getSelectedIndex());
                    pstIns.setInt(2, siloList.getSelectedIndex());
                    pstIns.setString(3, date);
                    int result = pstIns.executeUpdate();                        // Checks result of action.
                    System.out.println("siloListId = " + siloList.getSelectedIndex());
                    // Confirmation.
                    if (result == 1) {
                        System.out.println("Updated =" + result);
                    } else {
                        System.out.println("Not updated =" + result);
                    }
                }
                if (src == probesBtn) {
                    String probesInsert = "INSERT INTO Info (Id,SiloId,Probes) VALUES (?,?,?)"
                            + " ON DUPLICATE KEY UPDATE Probes = VALUES (Probes)";
                    PreparedStatement pstIns = con.prepareStatement(probesInsert);
                    pstIns.setInt(1, siloList.getSelectedIndex());
                    pstIns.setInt(2, siloList.getSelectedIndex());
                    pstIns.setString(3, date);
                    int result = pstIns.executeUpdate();
                    // Confirmation.
                    if (result == 1) {
                        System.out.println("Not updated =" + result);
                    } else {
                        System.out.println("Updated =" + result);
                    }

                }
                if (src == internScrBtn) {
                    String internScrewInsert = "INSERT INTO Info (Id,SiloId,InternalScrew) VALUES (?,?,?)"
                            + " ON DUPLICATE KEY UPDATE InternalScrew = VALUES (InternalScrew)";
                    PreparedStatement pstIns = con.prepareStatement(internScrewInsert);
                    pstIns.setInt(1, siloList.getSelectedIndex());
                    pstIns.setInt(2, siloList.getSelectedIndex());
                    pstIns.setString(3, date);
                    int result = pstIns.executeUpdate();
                    // Confirmation.
                    if (result == 1) {
                        System.out.println("Not updated =" + result);
                    } else {
                        System.out.println("Updated =" + result);
                    }
                }
                if (src == externScrBtn) {
                    String externalScrewInsert = "INSERT INTO Info (Id,SiloId,ExternalScrew) VALUES (?,?,?)"
                            + " ON DUPLICATE KEY UPDATE ExternalScrew = VALUES (ExternalScrew)";
                    PreparedStatement pstIns = con.prepareStatement(externalScrewInsert);
                    pstIns.setInt(1, siloList.getSelectedIndex());
                    pstIns.setInt(2, siloList.getSelectedIndex());
                    pstIns.setString(3, date);
                    int result = pstIns.executeUpdate();
                    // Confirmation.
                    if (result == 1) {
                        System.out.println("Not updated =" + result);
                    } else {
                        System.out.println("Updated =" + result);
                    }
                }
                if (src == checkEngineBtn) {
                    String checkEngineInsert = "INSERT INTO Info (Id,SiloId,Engine) VALUES (?,?,?)"
                            + " ON DUPLICATE KEY UPDATE Engine = VALUES (Engine)";
                    PreparedStatement pstIns = con.prepareStatement(checkEngineInsert);
                    pstIns.setInt(1, siloList.getSelectedIndex());
                    pstIns.setInt(2, siloList.getSelectedIndex());
                    pstIns.setString(3, date);
                    int result = pstIns.executeUpdate();
                    // Confirmation.
                    if (result == 1) {
                        System.out.println("Not updated =" + result);
                    } else {
                        System.out.println("Updated =" + result);
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(SiloUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    class SQLRecordsToLabels extends SiloParams {

        String urlChange = "jdbc:mysql://localhost:3306/Assets";

        @Override
        public void actionPerformed(ActionEvent e) {

            try (Connection con = DriverManager.getConnection(urlChange, user, pass)) {

                int siloNUM = siloList.getSelectedIndex();
                System.out.println("Selected List index =" + siloNUM);          // CHECKing.

                // Cleaned.
                String getCleaned = "SELECT Cleaned FROM Info WHERE Id = (?)";
                PreparedStatement pstClean = con.prepareStatement(getCleaned);
                pstClean.setInt(1, siloNUM);
                ResultSet rsClean = pstClean.executeQuery();
                cleanupLbl.setFont(new Font("Arial", Font.PLAIN, 16));
                if (rsClean.next()) {
                    cleanupLbl.setText(rsClean.getString(1));
                    cleanupLbl.setToolTipText("Почистване");
                    cleanupLbl.setHorizontalAlignment(JLabel.CENTER);
                }
                // Probes.
                String getProbes = "SELECT Probes FROM Info WHERE Id = (?)";
                PreparedStatement pstProbes = con.prepareStatement(getProbes);
                pstProbes.setInt(1, siloNUM);
                ResultSet rsProbes = pstProbes.executeQuery();
                probesLbl.setFont(new Font("Arial", Font.PLAIN, 16));
                if (rsProbes.next()) {
                    probesLbl.setText(rsProbes.getString(1));
                    probesLbl.setForeground(Color.black);
                    probesLbl.setToolTipText("Сонди");
                    probesLbl.setHorizontalAlignment(JLabel.CENTER);
                }
                // Internal Screw.
                String getInternalScr = "SELECT InternalScrew FROM Info WHERE Id = (?)";
                PreparedStatement pstInternal = con.prepareStatement(getInternalScr);
                pstInternal.setInt(1, siloNUM);
                ResultSet rsInternal = pstInternal.executeQuery();
                internScrewLbl.setFont(new Font("Arial", Font.PLAIN, 16));
                if (rsInternal.next()) {
                    internScrewLbl.setText(rsInternal.getString(1));
                    internScrewLbl.setToolTipText("Вътрешен шнек");
                    internScrewLbl.setForeground(Color.black);
                    internScrewLbl.setHorizontalAlignment(JLabel.CENTER);
                }
                // External Screw.
                String getExternalScr = "SELECT ExternalScrew FROM Info WHERE Id = (?)";
                PreparedStatement pstExternal = con.prepareStatement(getExternalScr);
                pstExternal.setInt(1, siloNUM);
                ResultSet rsExternal = pstExternal.executeQuery();
                externScrewLbl.setFont(new Font("Arial", Font.PLAIN, 16));
                if (rsExternal.next()) {
                    externScrewLbl.setText(rsExternal.getString(1));
                    externScrewLbl.setToolTipText("Външен шнек");
                    externScrewLbl.setHorizontalAlignment(JLabel.CENTER);
                }
                // Engine.
                String getEngine = "SELECT Engine FROM Info WHERE Id = (?)";
                PreparedStatement pstEngine = con.prepareStatement(getEngine);
                pstEngine.setInt(1, siloNUM);
                ResultSet rsEngine = pstEngine.executeQuery();
                checkEngineLbl.setFont(new Font("Arial", Font.PLAIN, 16));
                if (rsEngine.next()) {
                    checkEngineLbl.setText(rsEngine.getString(1));
                    checkEngineLbl.setToolTipText("Двигател");
                    checkEngineLbl.setHorizontalAlignment(JLabel.CENTER);
                }

            } catch (SQLException ex) {
                Logger.getLogger(SiloUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

public class AssetMaintenance extends SiloUI {

    JFrame mainArea = new JFrame("Silo Management");
    SiloBackground backgrnd = new SiloBackground();

    public static void main(String[] args) {
        // TODO code application logic here
        new AssetMaintenance();
    }

    public AssetMaintenance() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.getSystemLookAndFeelClassName();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                mainArea.setLayout(new BorderLayout());
                mainArea.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                backgrnd.setLayout(null);
                backgrnd.setBorder(new BevelBorder(BevelBorder.RAISED));
                backgrnd.add(externScrewLbl);
                backgrnd.add(internScrewLbl);
                backgrnd.add(probesLbl);
                backgrnd.add(checkEngineLbl);
                backgrnd.add(cleanupLbl);
                menubar.setBorder(new BevelBorder(BevelBorder.RAISED));
                mainArea.add(backgrnd, BorderLayout.CENTER);
                mainArea.add(allControlsPnl, BorderLayout.EAST);
                mainArea.add(menubar, BorderLayout.NORTH);
                mainArea.add(dateTimeFooter, BorderLayout.SOUTH);
                mainArea.pack();
                mainArea.setLocationRelativeTo(null);
                mainArea.setVisible(true);
                newDatabase();

            }
        });
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainArea.dispose();
                System.exit(0);
            }
        });
    }

    public class SiloBackground extends JPanel {

        Image img;

        public SiloBackground() {                                               // Silo pic background.
            try {
                img = ImageIO.read(getClass()
                        .getClassLoader()
                        .getResource("com/yavv/vitaaps/assetmaintenance/resources/smallsilo.png"));
            } catch (IOException ex) {
                Logger.getLogger(AssetMaintenance.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            if (img != null) {
                g.drawImage(img, 0, 0, null);
            }
        }

        @Override
        public Dimension getPreferredSize() {
            return img == null ? super.getPreferredSize() : new Dimension(img.getWidth(this), img.getHeight(this));
        }
    }
}
